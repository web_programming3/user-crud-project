import { ref, computed, onMounted } from "vue";
import { defineStore } from "pinia";
import { useUserStore } from "./user";
import { useMessageStore } from "./message";
export const useLoginStore = defineStore("login", () => {
  const loginName = ref("");
  const messageStore = useMessageStore();
  const islogin = computed(() => {
    // login name is not empty
    return loginName.value !== "";
  });
  const userStore = useUserStore();
  const login = (userName: string, password: string): void => {
    if (userStore.login(userName, password)) {
      loginName.value = userName;
      localStorage.setItem("loginName", userName);
    } else {
      messageStore.showMessage("Login or Password is incorrect");
    }
  };
  const logout = () => {
    loginName.value = "";
    localStorage.removeItem("loginName");
  };
  const loadData = () => {
    loginName.value = localStorage.getItem("loginName") || "";
  };

  return { loginName, islogin, login, logout, loadData };
});
